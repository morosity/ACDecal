// DecalSupportLibrariesDlg.h : header file
//

#pragma once

class CDecalSupportLibrariesDlgAutoProxy;


// CDecalSupportLibrariesDlg dialog
class CDecalSupportLibrariesDlg : public CDialog
{
	DECLARE_DYNAMIC(CDecalSupportLibrariesDlg);
	friend class CDecalSupportLibrariesDlgAutoProxy;

// Construction
public:
	CDecalSupportLibrariesDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CDecalSupportLibrariesDlg();

// Dialog Data
	enum { IDD = IDD_DECALSUPPORTLIBRARIES_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	CDecalSupportLibrariesDlgAutoProxy* m_pAutoProxy;
	HICON m_hIcon;

	BOOL CanExit();

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	DECLARE_MESSAGE_MAP()
};
