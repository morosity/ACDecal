// ScriptPlugin.h : Declaration of the cScriptPlugin

#ifndef __SCRIPTPLUGIN_H_
#define __SCRIPTPLUGIN_H_

#include "resource.h"       // main symbols
#include "PlainTextCP.h"

#include <DecalImpl.h>
#include "ScriptSiteImpl.h"

class CScriptView;

/////////////////////////////////////////////////////////////////////////////
// cScriptPlugin
class ATL_NO_VTABLE cScriptPlugin : 
	public CComObjectRootEx<CComMultiThreadModel>,
   public CComCoClass<cScriptPlugin, &CLSID_ScriptPlugin>,
   public IActiveScriptSiteImpl,
   public IPlugin2,
   public IDecalSurrogate,
   public IProvideClassInfo2Impl< &CLSID_ScriptPlugin, &DIID_IPluginEvents, &LIBID_PlainText >,
   public IDispatchImpl< IScriptPluginSite, &IID_IScriptPluginSite, &LIBID_PlainText >,
   public IDecalFileSurrogateXMLImpl< cScriptPlugin, &CLSID_ScriptPlugin >,
   public IConnectionPointContainerImpl<cScriptPlugin>,
   public CProxyIPluginEvents< cScriptPlugin >
{
public:
	cScriptPlugin()
	{
	}

   static LPCTSTR getConfigGroup()
   {
      return _T( "Plugins" );
   }

DECLARE_REGISTRY_RESOURCEID(IDR_SCRIPTPLUGIN)
DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cScriptPlugin)
   COM_INTERFACE_ENTRY(IActiveScriptSite)
   COM_INTERFACE_ENTRY(IPlugin2)
   COM_INTERFACE_ENTRY(IDecalFileSurrogate)
   COM_INTERFACE_ENTRY(IDecalSurrogate)
   COM_INTERFACE_ENTRY(IScriptPluginSite)
   COM_INTERFACE_ENTRY(IDispatch)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
   COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cScriptPlugin)
CONNECTION_POINT_ENTRY(DIID_IPluginEvents)
END_CONNECTION_POINT_MAP()

   CComPtr< IPluginSite2 > m_pSite;
   MSXML::IXMLDOMDocumentPtr m_pPlugin;

   void getCLSID( BSTR *pbstrCLSID );
   void getLanguage( BSTR *pbstrLanguage );

   HRESULT LoadScript( BSTR strFilename );

   CComBSTR m_strFilename;

   void postRemoveView( CScriptView *pView );
   void removeView( CScriptView * );

   typedef std::deque< CScriptView * > cViewList;
   cViewList m_views,
      m_destroy;
public:
	STDMETHOD(CreateView)(BSTR strTemplateName, /*[out, retval]*/ LPDISPATCH *ppNewView);
	STDMETHOD(LoadView)(/*[in]*/ BSTR Text, /*[out]*/ IView **pView);
	IPluginSite * getPluginSite();
	STDMETHOD(MessageBox)(BSTR Title, BSTR Text);
	STDMETHOD(WriteToChatWindow)(BSTR szText, long lColor);
   // IScriptPluginSite Methods
   STDMETHOD(CreateObject)(BSTR strProgID, LPDISPATCH *pDisp);

   // IDecalSurrogate Methods
	STDMETHOD(CreateInstance)(IDecalEnum *pInitData, REFIID iid, LPVOID *pObject);

   // IPlugin Methods
   STDMETHOD(Initialize)( IPluginSite2 *pSite );
   STDMETHOD(Terminate)();

   // IDecalFileSurrogate
   STDMETHOD(get_Extension)( BSTR *pVal )
   {
      if( pVal == NULL )
      {
         _ASSERT( FALSE );
         return E_POINTER;
      }

      *pVal = T2BSTR( _T( "acs" ) );
      return S_OK;
   }

   STDMETHOD(get_Description)( BSTR *pVal )
   {
      if( pVal == NULL )
      {
         _ASSERT( FALSE );
         return E_POINTER;
      }

      *pVal = T2BSTR( _T( "Script Plugin" ) );
      return S_OK;
   }
};

#endif //__SCRIPTPLUGIN_H_
