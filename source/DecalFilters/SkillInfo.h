// SkillInfo.h : Declaration of the cSkillInfo

#ifndef __SKILLINFO_H_
#define __SKILLINFO_H_

#include "resource.h"       // main symbols

#include "CharacterStats.h"

/////////////////////////////////////////////////////////////////////////////
// cSkillInfo
class ATL_NO_VTABLE cSkillInfo : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cSkillInfo, &CLSID_SkillInfo>,
	public IDispatchImpl<ISkillInfo, &IID_ISkillInfo, &LIBID_DecalFilters>
{
public:
	cSkillInfo()
	{
		m_pSkill = &m_pSkillInfo;
	}

   cCharacterStats *m_pStats;
   cCharacterStats::cSkillInfo m_pSkillInfo;
   cCharacterStats::cSkillInfo *m_pSkill;

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cSkillInfo)
	COM_INTERFACE_ENTRY(ISkillInfo)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// ISkillInfo
public:
	STDMETHOD(get_Known)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(get_Training)(/*[out, retval]*/ enum eTrainingType *pVal);
	STDMETHOD(get_Exp)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Current)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Base)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Increment)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Formula)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_ShortName)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_Name)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_Bonus)(/*[out, retval]*/ long *pVal);
};

#endif //__SKILLINFO_H_
