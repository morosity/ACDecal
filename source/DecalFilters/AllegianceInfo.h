// AllegianceInfo.h: Definition of the AllegianceInfo class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ALLEGIANCEINFO_H__5CFBBCEE_BF69_4C0A_984D_6694471DD508__INCLUDED_)
#define AFX_ALLEGIANCEINFO_H__5CFBBCEE_BF69_4C0A_984D_6694471DD508__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols
#include "CharacterStats.h"

/////////////////////////////////////////////////////////////////////////////
// AllegianceInfo

class cAllegianceInfo : 
	public IDispatchImpl<IAllegianceInfo, &IID_IAllegianceInfo, &LIBID_DecalFilters>, 
	public CComObjectRoot,
	public CComCoClass<cAllegianceInfo,&CLSID_AllegianceInfo>
{
public:

	cAllegianceInfo()
	{
		m_pAllegiance = &m_pAlleg;
	}

   cCharacterStats::cAllegianceInfo m_pAlleg;
   cCharacterStats::cAllegianceInfo *m_pAllegiance;

BEGIN_COM_MAP(cAllegianceInfo)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IAllegianceInfo)
END_COM_MAP()

//DECLARE_REGISTRY_RESOURCEID(IDR_AllegianceInfo)

// IAllegianceInfo
public:
	STDMETHOD(get_Unknown)(/*[out, retval]*/ double *pVal);
	STDMETHOD(get_Rank)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Race)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Gender)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Leadership)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Loyalty)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_XP)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Type)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_TreeParent)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_GUID)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Name)(/*[out, retval]*/ BSTR *pVal);
};

#endif // !defined(AFX_ALLEGIANCEINFO_H__5CFBBCEE_BF69_4C0A_984D_6694471DD508__INCLUDED_)
