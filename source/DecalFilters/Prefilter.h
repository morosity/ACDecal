// Prefilter.h : Declaration of the cPrefilter

#ifndef __PREFILTER_H_
#define __PREFILTER_H_

#include "resource.h"       // main symbols

#include <Decal.h>
#include <DecalNetImpl.h>
#include <DecalImpl.h>
#include "DecalFiltersCP.h"

/////////////////////////////////////////////////////////////////////////////
// cPrefilter
class ATL_NO_VTABLE cPrefilter : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<cPrefilter, &CLSID_Prefilter>,
	public IDispatchImpl<IPrefilter, &IID_IPrefilter, &LIBID_DecalFilters>,
   public ISoloNetworkFilterImpl<cPrefilter>,
   public IDecalSurrogate,
   public IProvideClassInfo2Impl<&CLSID_Prefilter, &DIID_IPrefilterEvents, &LIBID_DecalFilters >,
   public IDecalFileSurrogateXMLImpl< cPrefilter, &CLSID_Prefilter >,
   public CProxyIPrefilterEvents< cPrefilter >,
   public IConnectionPointContainerImpl<cPrefilter>
{
public:
   static LPCTSTR getConfigGroup()
   {
      return _T( "NetworkFilters" );
   }

	cPrefilter()
	{
	}

   class cFilterRule
   {
   public:
      long m_nFire;

      enum { eFireChildren = -1 };

      typedef std::vector< VSBridge::auto_ptr< cFilterRule > > cFilterRuleList;
      cFilterRuleList m_childRules;

      cFilterRule()
         : m_nFire( eFireChildren )
      {
      }

      virtual ~cFilterRule()
      {
      }

      virtual bool test( IMessageIterator *pMsg )
      {
         return true;
      }
   };

   typedef std::map< long, VSBridge::auto_ptr< cFilterRule > > cMessageRuleList;
   cMessageRuleList m_messageRules;
   _bstr_t m_strFile;

   bool testRules( cFilterRule *pRule, IMessageIterator *pMsg );
   cFilterRule *loadRule( MSXML::IXMLDOMElementPtr &pElement );

DECLARE_REGISTRY_RESOURCEID(IDR_PREFILTER)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cPrefilter)
   COM_INTERFACE_ENTRY(IDecalSurrogate)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
   COM_INTERFACE_ENTRY(INetworkFilter2)
	COM_INTERFACE_ENTRY(IPrefilter)
	COM_INTERFACE_ENTRY(IDispatch)
   COM_INTERFACE_ENTRY(IDecalFileSurrogate)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cPrefilter)
	CONNECTION_POINT_ENTRY(DIID_IPrefilterEvents)
END_CONNECTION_POINT_MAP()

// IPrefilter
public:

   // INetworkFilter
   STDMETHOD(Initialize)(INetService *pService);
   STDMETHOD(DispatchServer)(IMessage2 *pMsg);

   // IDecalSurrogate
	STDMETHOD(CreateInstance)(IDecalEnum *pInitData, REFIID iid, LPVOID *pObject);

   // IDecalFileSurrogate
   STDMETHOD(get_Extension)( BSTR *pVal )
   {
      if( pVal == NULL )
      {
         _ASSERT( FALSE );
         return E_POINTER;
      }

      *pVal = T2BSTR( _T( "apn" ) );
      return S_OK;
   }

   STDMETHOD(get_Description)( BSTR *pVal )
   {
      if( pVal == NULL )
      {
         _ASSERT( FALSE );
         return E_POINTER;
      }

      *pVal = T2BSTR( _T( "Network Prefilter" ) );
      return S_OK;
   }
};

#endif //__PREFILTER_H_
