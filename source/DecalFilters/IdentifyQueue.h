// IdentifyQueue.h : Declaration of the CIdentifyQueue

#ifndef __IDQUEUE_H_
#define __IDQUEUE_H_

#pragma once
#include "resource.h"       // main symbols

#include "DecalFilters.h"
#include "DecalNetImpl.h"
#include "..\include\decal.h"
#include <deque>

typedef void(*RequestPtr)( long );

struct IDStruct
{
	long lObjectID;
	long lTime;
};

// CIdentifyQueue

class ATL_NO_VTABLE CIdentifyQueue : 
	public IKitchenSink,
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CIdentifyQueue, &CLSID_IdentifyQueue>,
	public IDispatchImpl<IIdentifyQueue, &IID_IIdentifyQueue, &LIBID_DecalFilters, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public INetworkFilterImpl<CIdentifyQueue>
{
public:
	CIdentifyQueue()
	{
		m_bWaiting = false;
		m_lAttempts = 0;
		m_lLastManualAttempt = 0;
	}

DECLARE_REGISTRY_RESOURCEID(IDR_IDENTIFYQUEUE)
DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CIdentifyQueue)
	COM_INTERFACE_ENTRY(IIdentifyQueue)
	COM_INTERFACE_ENTRY(INetworkFilter2)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

private:
	CComPtr< IDecal >					m_pDecal;
	CComPtr< IACHooks >					m_pHooks;
    
    typedef std::deque< IDStruct * >	IDContainer;

	IDContainer							m_Queue;
	bool								m_bWaiting;
	long								m_lAttempts;
	long								m_lLastManualAttempt;

	// Id Func pointer
	RequestPtr							m_pfRequestFunc;
    
	void		MsgGameEvent( IMessageIterator *pMembers );
	void		MsgIDItem( IMessageIterator *pMembers );

	void		Request();

public:
	// INetworkFilterImpl
	HRESULT onInitialize();
	HRESULT onTerminate();

	// INetworkFilter
	STDMETHOD(DispatchServer)( IMessage2 *pMsg );

	// IKitchenSink
	STDMETHOD(AddToQueue)( long lObjectID );
	STDMETHOD(ShortcircuitID)( long lObjectID );
};

#endif //__IDQUEUE_H_