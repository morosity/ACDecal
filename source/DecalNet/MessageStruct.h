// MessageStruct.h : Declaration of the cMessageStruct

#ifndef __MESSAGESTRUCT_H_
#define __MESSAGESTRUCT_H_

#include "resource.h"       // main symbols
#include "MessageImpl.h"

/////////////////////////////////////////////////////////////////////////////
// cMessageStruct
class ATL_NO_VTABLE cMessageStructIter : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public IDispatchImpl<IMessageMember, &IID_IMessageMember, &LIBID_DecalNet>,
	public IMessageIteratorSublistImpl
{
public:
	cMessageStructIter()
	{
	}

BEGIN_COM_MAP(cMessageStructIter)
	COM_INTERFACE_ENTRY(IMessageIterator)
	COM_INTERFACE_ENTRY(IMessageMember)
	COM_INTERFACE_ENTRY2(IDispatch, IMessageMember)
END_COM_MAP()

public:
   // IMessageStruct Implementation
	STDMETHOD(get_Count)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_MemberName)(long Index, /*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_Member)(VARIANT vIndex, /*[out, retval]*/ VARIANT *pVal);
};

#endif //__MESSAGESTRUCT_H_
