// WebRequest.h : Declaration of the cWebRequest

#ifndef __WEBREQUEST_H_
#define __WEBREQUEST_H_

#include "resource.h"       // main symbols
#include "DecalNetCP.h"

/////////////////////////////////////////////////////////////////////////////
// cWebRequest
class ATL_NO_VTABLE cWebRequest : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cWebRequest, &CLSID_WebRequest>,
	public IConnectionPointContainerImpl<cWebRequest>,
   public IBindStatusCallback,
   public IHttpNegotiate,
   public IProvideClassInfo2Impl< &CLSID_WebRequest, &DIID_IWebRequestEvents, &LIBID_DecalNet >,
	public IDispatchImpl<IWebRequest, &IID_IWebRequest, &LIBID_DecalNet>,
	public CProxyIWebRequestEvents< cWebRequest >
{
public:
	cWebRequest()
	{
	}

   _bstr_t m_strResult;
   _bstr_t m_strPost;

   CComPtr< IBinding > m_pBinding;

DECLARE_REGISTRY_RESOURCEID(IDR_WEBREQUEST)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cWebRequest)
   COM_INTERFACE_ENTRY(IBindStatusCallback)
	COM_INTERFACE_ENTRY(IWebRequest)
	COM_INTERFACE_ENTRY(IDispatch)
   COM_INTERFACE_ENTRY(IHttpNegotiate)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cWebRequest)
   CONNECTION_POINT_ENTRY(DIID_IWebRequestEvents)
END_CONNECTION_POINT_MAP()

// IWebRequest
public:
	STDMETHOD(Post)(BSTR strURL, BSTR strPostData);
	STDMETHOD(Get)(BSTR strURL);

// IBindStatusCallback
   STDMETHOD(GetBindInfo)(DWORD *pgrfBINDF, BINDINFO *pbindinfo);
   STDMETHOD(GetPriority)(LONG *pnPriority)
   {
      *pnPriority = THREAD_PRIORITY_NORMAL;
      return S_OK;
   }

   STDMETHOD(OnDataAvailable)(DWORD grfBSCF, DWORD dwSize, FORMATETC *pfmtetc, STGMEDIUM *pstgmed);
   STDMETHOD(OnLowResource)(DWORD)
   {
      return E_NOTIMPL;
   }
   STDMETHOD(OnObjectAvailable)(REFIID, IUnknown *)
   {
      return E_NOTIMPL;
   }
   STDMETHOD(OnProgress)(ULONG, ULONG, ULONG, LPCWSTR)
   {
      return E_NOTIMPL;
   }
   STDMETHOD(OnStartBinding)(DWORD, IBinding *pBinding)
   {
      m_pBinding = pBinding;

      // Reset the result
      m_strResult = _bstr_t( _T( "" ) );
      Fire_Begin();

      return S_OK;
   }
   STDMETHOD(OnStopBinding)(HRESULT hr, LPCWSTR);

// IHttpNegotiate
   STDMETHOD(BeginningTransaction)(LPCWSTR szURL, LPCWSTR szHeaders, DWORD, LPWSTR *pszAdditionalRequestHeaders);
   STDMETHOD(OnResponse)(DWORD dwResponseCode, LPCWSTR szResponseHeaders, LPCWSTR szRequestHeaders, LPWSTR *pszAdditionalRequestHeaders);
};

#endif //__WEBREQUEST_H_
