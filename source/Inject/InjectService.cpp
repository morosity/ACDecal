// InjectService.cpp : Implementation of cInjectService
#include "stdafx.h"
#include "Inject.h"
#include "InjectService.h"

#include "Manager.h"

/////////////////////////////////////////////////////////////////////////////
// cInjectService

STDMETHODIMP cInjectService::BeforePlugins ()
{
   _ASSERTE ( cManager::_p != NULL );
   cManager::_p->init ();

   return S_OK;
}

STDMETHODIMP cInjectService::AfterPlugins ()
{
   _ASSERTE ( cManager::_p != NULL );

   cManager::_p->unloadPlugins ();
   cManager::_p->term ();

   return S_OK;
}

STDMETHODIMP cInjectService::get_Site(IPluginSite **pVal)
{
   _ASSERTE ( cManager::_p != NULL );
   return static_cast< IPluginSite * > ( cManager::_p )->QueryInterface ( pVal );
}

STDMETHODIMP cInjectService::InitPlugin(IUnknown *pUnk)
{
   _ASSERTE ( cManager::_p != NULL );

   cManager::_p->loadPlugin ( pUnk );

	return S_OK;
}
