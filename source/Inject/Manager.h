// Manager.h : Declaration of the cManager

#ifndef __MANAGER_H_
#define __MANAGER_H_

#include "resource.h"       // main symbols

#include "LayerSite.h"
#include "BarLayer.h"
#include "DatFile.h"
#include "Panel.h"
#include <Decal.h>
#include "..\Decal\ACHooks.h"

class cIconCache;
class cImage;
class cFontCache;


#define ChatTextHook		102
#define ChatMessageHook		103
extern const IID EVTID_AcHooks ;
// Template class used to connect ACHooks events
template<UINT nID, class cImpl >class IACHooksEventsImpl
:public IDispEventImpl<nID, cImpl, &EVTID_AcHooks, &LIBID_Decal, 1, 0 >
{
public:	HRESULT advise(IUnknown *pUnk){return DispEventAdvise(pUnk);}
		HRESULT unadvise(IUnknown *pUnk){return DispEventUnadvise(pUnk);}
};

/////////////////////////////////////////////////////////////////////////////
// cManager
class ATL_NO_VTABLE cManager : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public IACHooksEventsImpl<ChatMessageHook, cManager>,
	public IACHooksEventsImpl<ChatTextHook, cManager>,
	public IPluginSite
{
public:
	cManager();
   ~cManager();

   void setDirectDraw( IDirectDraw4 *pDD, IDirect3D3 *pD3D, IDirectDrawSurface4 *pDDS );
   void setSurface( IDirect3DDevice3 *pDevice );

   void init();
   void term();

   void loadPlugin ( IUnknown *pUnk );
   void unloadPlugins ();
   void setWindow( HWND hWnd );

   void draw2D();
   void draw3D();
   void sendPreBeginScene();
   void sendPostBeginScene();
   void sendPreEndScene();
   void sendPostEndScene();
   void clearDestroyList();

   void __stdcall onChatMessage(BSTR bstrText, long lColor, VARIANT_BOOL *pbEat);
   void __stdcall onChatText(BSTR bstrText, VARIANT_BOOL *pbEat);

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cManager)
   COM_INTERFACE_ENTRY(IPluginSite)
END_COM_MAP()

BEGIN_SINK_MAP(cManager)
	SINK_ENTRY_EX( ChatMessageHook, EVTID_AcHooks, 2, onChatMessage)
	SINK_ENTRY_EX( ChatTextHook, EVTID_AcHooks, 3, onChatText)
END_SINK_MAP()

   class cMemLocs
	{
	public:
		_bstr_t bstrName;
		DWORD dwValue;	
	};

   // Data members for the plugin objects
   class cPlugin
   {
   public:
      DWORD m_dwSinkCaps;
      IUnknown *m_pPlugin;
   };

   long m_nNextPlugin;
   typedef std::list< cPlugin > cPluginList;
   cPluginList m_plugins;

   // Data for windowing

   // The root window pair
   cLayerSite *m_pRootSite;

   // Various capture information
   cLayerSite *m_pMouseOver,
      *m_pMouseDown,
      *m_pKeyboard;

   // Popup recoil structures
   typedef std::list< cLayerSite * > cLayerList;
   cLayerList m_popups;

   // Destruction queue
   cLayerList m_destroy;

   // Icon Cache Management
   typedef std::list< cIconCache * > cIconsList;
   cIconsList m_icons;

   // Image Cache Management
   typedef std::list< cImage * > cImagesList;
   cImagesList m_images;

   void removeImage( cImage * );

   // Font Cache Management
   typedef std::list< cFontCache * > cFontsList;
   cFontsList m_fonts;

   void removeFont( cFontCache * );

   // AC Data Files
   cDatFile m_portal;

   // Hooked stuff
   HWND m_hMain;
   WNDPROC m_pfnOld;
   DWORD m_dwROT;
   //HANDLE m_hDrawSync;
   bool m_bInitialized;
   bool m_bContainer;
   bool m_bXMLViewViewer;
   cRootLayer* m_pRootLayer;

   DWORD m_dwChatMessageAddy;
 
   long m_lPrevSelKey;

   CComPtr< IACHooks > m_pHooks;

   eAlphaBlendOptions m_eAlphaBlendMode;

  // std::list< LPDIRECTDRAWSURFACE4 > SurfaceList;

   IDirectDraw4 *m_pD;
   IDirect3D3 *m_p3D;
   IDirect3DDevice3 *m_p3DDevice;
   IDirectDrawSurface4 *m_pPrimarySurface;
   
   LPVOID m_lpSurface;

   HMODULE m_hDecalDLL;
   bool m_bSoftware;
   
public:
   static CComObject< cManager > *_p;
   CComPtr< IDecal > m_pDecal;

public:
   LRESULT localWndProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam );
   static LRESULT CALLBACK wndProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam );

   void createSurface( LPSIZE psz, IDirectDrawSurface4 **ppSurf );
   void convertVersion( LPTSTR szVersion, DWORD &dwVersionMajor, DWORD &dwVersionMinor );
   void enableSoftwareMode( );

public:
	STDMETHOD(get_Hooks)(/*[out, retval]*/ IACHooks * *pVal);
	STDMETHOD(get_Focus)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(get_HWND)(/*[out, retval]*/ long *pVal);

   // IPluginSite Methods
	STDMETHOD(CreateCanvas)(LPSIZE psz, /*[out, retval]*/ ICanvas **ppCanvas);
	STDMETHOD(GetScreenSize)(LPSIZE sz);
	STDMETHOD(CreateFont)(BSTR strFaceName, long nHeight, long nFlags, /*[out, retval]*/ IFontCache **ppFont);
	STDMETHOD(LoadBitmapPortal)(long nFile, IImageCache **);
	STDMETHOD(GetIconCache)(LPSIZE sz, /*[out, retval]*/ IIconCache **ppCache);
	STDMETHOD(LoadBitmapFile)(BSTR strFilename, IImageCache **ppImage);
	STDMETHOD(Get3DDevice)(REFIID iid, void **ppvItf);
	STDMETHOD(GetPrimarySurface)(/*[out, retval]*/ ICanvas **ppCanvas);
	STDMETHOD(GetDirectDraw)(REFIID iid, void **ppvItf);
	STDMETHOD(UnloadPlugin)(long nID);
	STDMETHOD(CreateView)(ViewParams *pParams, ILayer *pLayer, /*[out, retval]*/ IView **ppView);
	STDMETHOD(LoadView)(BSTR strSchema, /*[out, retval]*/ IView **ppView);
	STDMETHOD(CreateFontSchema)(long nDefHeight, long nDefOptions, IUnknown *pSchema, /*[out, retval]*/ IFontCache **ppCache);
	STDMETHOD(LoadImageSchema)(IUnknown *pSchema, /*[out, retval]*/ IImageCache **ppImg);
	STDMETHOD(CreateBrushImage)(long nColor, /*[out, retval]*/ IImageCache **ppImg);
	STDMETHOD(LoadViewObject)(IUnknown *pSchema, /*[out, retval]*/ IView **ppView);
	STDMETHOD(get_NetworkFilter)(BSTR strProgID, /*[out, retval]*/ LPDISPATCH *pVal);
	STDMETHOD(get_Plugin)(BSTR strProgID, /*[out, retval]*/ LPDISPATCH *pVal);
	STDMETHOD(get_ResourcePath)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(LoadResourceModule)(BSTR strLibrary, /*[out, retval]*/ long *pnModule);
	STDMETHOD(CreateInputBuffer)(/*[out, retval]*/ IInputBuffer **ppInput);
	STDMETHOD(get_OldWndProc)(/*[out, retval]*/ long *pOldWndProc);

	STDMETHOD(put_CurrentSelection)(long nID);
	STDMETHOD(get_CurrentSelection)(long *nID);

	STDMETHOD(put_PreviousSelection)(long nID);
	STDMETHOD(get_PreviousSelection)(long *nID);

	STDMETHOD(WriteToChatWindow)(BSTR szText, long lColor);

	STDMETHOD(SetCursorPosition)(long x, long y);

	STDMETHOD(QueryKeyboardMap)(BSTR bstrName, long *pAsciiVal);

	STDMETHOD(QueryMemLoc)(BSTR bstrTag, long *pVal);

	STDMETHOD(RawWriteToChatWindow)(BSTR szText, long lColor);

	STDMETHOD(CastSpell)(long lSpellID, long lObjectID);	//0x004EFB10

	STDMETHOD(MoveItem)(long lObjectID, long lPackID, long lSlot, long lStack);		//0x4F9E80

	STDMETHOD(SelectItem)(long lObjectID);	//0x4CF850

	STDMETHOD(UseItem)(long lObjectID, long lUseOnSelectedItem);	//0x4FA9F0

	STDMETHOD(get_CombatState)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_ChatState)(/*[out, retval]*/ VARIANT_BOOL *pVal);

	STDMETHOD(UseItemEx)(long Use, long UseOn);
	STDMETHOD(GetFellowStats)(long charID);

	STDMETHOD(get_Decal)(IDecal **pVal);
	STDMETHOD(RedrawBar)();

	STDMETHOD(get_FontName)(BSTR *pFontName); 
};

#endif //__MANAGER_H_
