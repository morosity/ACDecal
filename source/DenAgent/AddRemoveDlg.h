#if !defined(AFX_ADDREMOVEDLG_H__CA058B98_7B3E_4315_BB99_0D8D582335BA__INCLUDED_)
#define AFX_ADDREMOVEDLG_H__CA058B98_7B3E_4315_BB99_0D8D582335BA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AddRemoveDlg.h : header file
//

#include <Decal.h>

/////////////////////////////////////////////////////////////////////////////
// cAddRemoveDlg dialog

typedef struct tagSortInfo
{
	CListCtrl *pListControl;
	int	nColumnNo;
	bool nAscendingSortOrder;
} SortInfo;

typedef CRuntimeClass * (*DLLREG)();

class CListViewSC : public CListCtrl
{
	friend class cAddRemoveDlg;

	public:
		CListViewSC(){};
		cAddRemoveDlg *m_pAddRemoveDlg;
		bool SearchDirectory(char *szDirectory);
		bool m_bGotAFile;
	protected:
		//{{AFX_MSG(CListViewSC)
		afx_msg void OnDropFiles(HDROP dropInfo);
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	private:
		CListViewSC& operator=(const CListViewSC& x);
		CListViewSC(const CListViewSC& x);
};

class cAddRemoveDlg : public CDialog
{
// Construction
public:
	cAddRemoveDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(cAddRemoveDlg)
	enum { IDD = IDD_ADDREMOVE };
	CListViewSC	m_wndPlugins;
	//}}AFX_DATA

   struct cPlugin
   {
      CLSID m_clsid;
      WCHAR m_szURL[ 1024 ];
      DWORD m_dwMajor;
      DWORD m_dwMinor;
      bool m_bInstalled;
      DWORD m_dwInstalledMajor;
      DWORD m_dwInstalledMinor;
	  bool m_bBinary;
   };

   CImageList m_ImageList;
   SortInfo m_SortInfo;

   bool m_bHide;

   typedef std::list< cPlugin > cPluginList;
   cPluginList m_plugins;
   
   IDecal *m_pDecal;

   MSXML::IXMLDOMDocumentPtr m_pDoc;

   void DisplayError(HRESULT hr);
   void convertVersion( LPTSTR szVersion, DWORD &dwVersionMajor, DWORD &dwVersionMinor );
   void loadListing();
   bool ReadTypeLib(CString cstrDll, bool bMultipleFiles);
   static int CALLBACK ComparePlugins(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(cAddRemoveDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(cAddRemoveDlg)
	afx_msg void OnChangeDir();
	afx_msg void OnInstall();
	afx_msg void OnDblclkPlugins(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnUpgrade();
	virtual BOOL OnInitDialog();
	afx_msg void OnColumnclickPlugins(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnInfoTip( NMHDR * pNMHDR, LRESULT * pResult );
	afx_msg void OnBrowse();
	afx_msg void OnRefresh();
	afx_msg void OnSelChanged(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADDREMOVEDLG_H__CA058B98_7B3E_4315_BB99_0D8D582335BA__INCLUDED_)
