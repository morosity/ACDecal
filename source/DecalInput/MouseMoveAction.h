// MouseMoveAction.h : Declaration of the cMouseMoveAction

#ifndef __MOUSEMOVEACTION_H_
#define __MOUSEMOVEACTION_H_

#include "resource.h"       // main symbols
#include <DecalInputImpl.h>

/////////////////////////////////////////////////////////////////////////////
// cMouseMoveAction
class ATL_NO_VTABLE cMouseMoveAction : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<cMouseMoveAction, &CLSID_MouseMoveAction>,
	public IInputActionImpl< cMouseMoveAction >
{
public:
	cMouseMoveAction()
	{
	}

   enum eDirection
   {
      eLeftTop,
      eRightBottom,
      ePercent
   };

   struct cValue
   {
      eDirection m_eDir;
      long m_nMagnitude;

      long value( long nMax )
      {
         switch( m_eDir )
         {
         case eLeftTop:
            return m_nMagnitude;

         case ePercent:
            return ( ( nMax * m_nMagnitude ) / 100 );

         case eRightBottom:
            return nMax - m_nMagnitude;
         }

         // Unknown type
         _ASSERT( FALSE );
         return 0;
      }
   };

   cValue m_x, m_y;

   HRESULT onLoad( LPTSTR szData );
   HRESULT loadValue( LPTSTR szData, cValue &val );

DECLARE_REGISTRY_RESOURCEID(IDR_MOUSEMOVEACTION)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cMouseMoveAction)
	COM_INTERFACE_ENTRY(IInputAction)
END_COM_MAP()

// IMouseMoveAction
public:
   STDMETHOD(Execute)();
};

#endif //__MOUSEMOVEACTION_H_
