// Checkbox.h : Declaration of the cCheckbox

#ifndef __CHECKBOX_H_
#define __CHECKBOX_H_

#include "resource.h"       // main symbols
#include "DecalControlsCP.h"

#include "SinkImpl.h"

/////////////////////////////////////////////////////////////////////////////
// cCheckbox
class ATL_NO_VTABLE cCheckbox : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cCheckbox, &CLSID_Checkbox>,
	public IControlImpl< cCheckbox, ICheckbox, &IID_ICheckbox, &LIBID_DecalControls >,
   public ILayerMouseImpl,
   public ILayerRenderImpl,
   public ILayerImpl< cCheckbox >,
   public ILayerSchema,
   public IProvideClassInfo2Impl< &CLSID_Checkbox, &DIID_ICheckboxEvents, &LIBID_DecalControls >,
	public IConnectionPointContainerImpl<cCheckbox>,
	public CProxyICheckboxEvents< cCheckbox >
{
public:
	cCheckbox();

   _bstr_t m_strText;
   CComPtr< IFontCache > m_pFont;

   VARIANT_BOOL m_bChecked,
      m_bRightToLeft;

   long m_nTextColor, m_nOutlineColor;
   bool m_bAA, m_bOutline;

   POINT m_ptText,
      m_ptCheck;

   VARIANT_BOOL m_bPressed,
      m_bMouseIn;

DECLARE_REGISTRY_RESOURCEID(IDR_CHECKBOX)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cCheckbox)
   COM_INTERFACE_ENTRY(ILayerRender)
   COM_INTERFACE_ENTRY(ILayerMouse)
   COM_INTERFACE_ENTRY(ILayerSchema)
	COM_INTERFACE_ENTRY(ICheckbox)
   COM_INTERFACE_ENTRY(IControl)
   COM_INTERFACE_ENTRY(IDispatch)
   COM_INTERFACE_ENTRY(ILayer)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cCheckbox)
CONNECTION_POINT_ENTRY(DIID_ICheckboxEvents)
END_CONNECTION_POINT_MAP()

public:
   // ICheckbox Methods
	STDMETHOD(get_RightToLeft)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(put_RightToLeft)(/*[in]*/ VARIANT_BOOL newVal);
	STDMETHOD(get_Checked)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(put_Checked)(/*[in]*/ VARIANT_BOOL newVal);
	STDMETHOD(get_TextColor)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_TextColor)(/*[in]*/ long newVal);
	STDMETHOD(get_Text)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Text)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Font)(/*[out, retval]*/ IFontCacheDisp * *pVal);
	STDMETHOD(putref_Font)(/*[in]*/ IFontCacheDisp * newVal);

   // ILayerSchema Methods
   STDMETHOD(SchemaLoad)(IView *pView, IUnknown *pSchema);

   // ILayerRender Methods
   STDMETHOD(Reformat)();
   STDMETHOD(Render)(ICanvas *pCanvas);

   // ILayerMouse Methods
	STDMETHOD(MouseEnter)(struct MouseState *);
	STDMETHOD(MouseExit)(struct MouseState *);
	STDMETHOD(MouseDown)(struct MouseState *);
	STDMETHOD(MouseUp)(struct MouseState *);
};

#endif //__CHECKBOX_H_
