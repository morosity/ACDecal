// Edit.h : Declaration of the cEdit

#ifndef __EDIT_H_
#define __EDIT_H_

#include "resource.h"       // main symbols

#include "SinkImpl.h"
#include "DecalControlsCP.h"

/////////////////////////////////////////////////////////////////////////////
// cEdit
class ATL_NO_VTABLE cEdit : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cEdit, &CLSID_Edit>,
	public IConnectionPointContainerImpl<cEdit>,
	public ILayerImpl< cEdit >,
	public ILayerRenderImpl,
	public ILayerMouseImpl,
	public ILayerKeyboard,
	public ILayerTimer,
	public ILayerSchema,
	public IControlImpl< cEdit, IEdit, &IID_IEdit, &LIBID_DecalControls >,
	public IProvideClassInfo2Impl< &CLSID_Edit, &DIID_IEditEvents, &LIBID_DecalControls >,
	public CProxyIEditEvents< cEdit >
{
public:
	cEdit();

   CComPtr< IImageCache > m_pBackground;
   CComPtr< IFontCache > m_pFont;
   SIZE m_szMargin;

   bool m_bAllowCapture; // cyn, 15/10/2002
   bool m_bCapture;
   bool m_bDrawCaret;
   bool m_bPassword;
   bool m_bSelecting;
   bool m_bOutline;
   bool m_bAA;

   long m_nCaretChar, m_nCaret, m_nOffset,
      m_nCaretHeight,
      m_nSelStart, m_nSelStartChar,
      m_nTextColor, m_nOutlineColor;



   std::string m_strText;
   std::string m_strPass;

   void checkFont();
   void sendTextChange();
   void Delete();
   void Copy();
   void Cut();
   void Paste();
   int Normalize(int Offset);

DECLARE_REGISTRY_RESOURCEID(IDR_EDIT)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cEdit)
   COM_INTERFACE_ENTRY(ILayerRender)
   COM_INTERFACE_ENTRY(ILayerMouse)
   COM_INTERFACE_ENTRY(ILayerKeyboard)
   COM_INTERFACE_ENTRY(ILayerTimer)
   COM_INTERFACE_ENTRY(ILayerSchema)
   COM_INTERFACE_ENTRY(ILayer)
	COM_INTERFACE_ENTRY(IEdit)
   COM_INTERFACE_ENTRY(IControl)
   COM_INTERFACE_ENTRY(IDispatch)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cEdit)
CONNECTION_POINT_ENTRY(DIID_IEditEvents)
END_CONNECTION_POINT_MAP()

public:
   // IEdit Methods
	STDMETHOD(get_SelectedText)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_SelectedText)(/*[in]*/ BSTR newVal);
	STDMETHOD(Select)(long nStart, long nEnd);
	STDMETHOD(get_Caret)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_Caret)(/*[in]*/ long newVal);
	STDMETHOD(get_Text)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Text)(/*[in]*/ BSTR newVal);
	STDMETHOD(SetMargins)(long nX, long nY);
	STDMETHOD(get_TextColor)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_TextColor)(/*[in]*/ long newVal);
	STDMETHOD(get_Font)(/*[out, retval]*/ IFontCacheDisp * *pVal);
	STDMETHOD(putref_Font)(/*[in]*/ IFontCacheDisp * newVal);
	STDMETHOD(get_Background)(/*[out, retval]*/ IImageCacheDisp * *pVal);
	STDMETHOD(putref_Background)(/*[in]*/ IImageCacheDisp * newVal);
	STDMETHOD(Capture)();

   // ILayerRender Methods
   STDMETHOD(Reformat)();
   STDMETHOD(Render)(ICanvas *pCanvas);

   // ILayerMouse Methods
   STDMETHOD(MouseDown)(MouseState *pMS);
   STDMETHOD(MouseUp)(MouseState *pMS);
   STDMETHOD(MouseMove)(MouseState *pMS);

   // ILayerKeyboard Methods
   STDMETHOD(KeyboardChar)(KeyState *pKS);
   STDMETHOD(KeyboardEndCapture)(VARIANT_BOOL bCancel);
   STDMETHOD(KeyboardEvent)( long uMsg, long wParam, long lParam );

   // ILayerTimer Methods
   STDMETHOD(TimerTimeout)(long nID, long nInterval, long nReps, VARIANT_BOOL *pbContinue);

   // ILayerSchema Methods
   STDMETHOD(SchemaLoad)(IView *pView, IUnknown *pSchema);
};

#endif //__EDIT_H_
