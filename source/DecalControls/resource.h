//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by DecalControls.rc
//
#define IDS_PROJNAME                    100
#define IDR_EDIT                        101
#define IDR_CHOICE                      102
#define IDR_CLIENT                      103
#define IDR_BORDERLAYOUT                105
#define IDR_PAGELAYOUT                  106
#define IDR_PUSHBUTTON                  107
#define IDR_STATIC                      108
#define IDR_CHECKBOX                    109
#define IDS_DERETHMAP_DESC              133
#define IDR_DerethMap                   134
#define IDR_PROGRESS                    136
#define IDR_TEXTCOLUMN                  201
#define IDR_ICONCOLUMN                  202
#define IDR_LIST                        203
#define IDR_NOTEBOOK                    204
#define IDR_SCROLLER                    205
#define IDR_CHECKCOLUMN                 206
#define IDR_FIXEDLAYOUT                 207
#define IDR_SLIDER                      208

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        209
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           137
#endif
#endif
