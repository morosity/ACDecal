// ChoicePopup.h : Declaration of the cChoicePopup

#ifndef __CHOICEPOPUP_H_
#define __CHOICEPOPUP_H_

#include "resource.h"       // main symbols

#include "SinkImpl.h"

class cChoice;

/////////////////////////////////////////////////////////////////////////////
// cChoicePopup
class ATL_NO_VTABLE cChoicePopup : 
	public CComObjectRootEx<CComMultiThreadModel>,
   public ILayerImpl< cChoicePopup >,
   public ILayerRenderImpl,
   public ILayerMouseImpl,
   public cNoEventsImpl,
	public IChoicePopup
{
public:
	cChoicePopup()
      : m_pChoice( NULL ),
      m_bMouseDown( false )
	{
	}

   cChoice *m_pChoice;
   CComPtr< IImageCache > m_pActive;
   bool m_bMouseDown;

   void onCreate();
   void onDestroy();

BEGIN_COM_MAP(cChoicePopup)
   COM_INTERFACE_ENTRY(ILayerRender)
   COM_INTERFACE_ENTRY(ILayerMouse)
   COM_INTERFACE_ENTRY(ILayer)
	COM_INTERFACE_ENTRY(IChoicePopup)
END_COM_MAP()

   void hotSelect( MouseState *pMS );

public:
   // IChoicePopup Methods

   // ILayerRender Methods
   STDMETHOD(Render)(ICanvas *pCanvas);

   // ILayerMouse Methods
   STDMETHOD(MouseDown)(MouseState *pMS)
   {
      m_bMouseDown = true;
      hotSelect( pMS );

      return S_OK;
   }

   STDMETHOD(MouseEnter)(MouseState *pMS)
   {
      if( m_bMouseDown )
         hotSelect( pMS );

      return S_OK;
   }

   STDMETHOD(MouseExit)(MouseState *pMS)
   {
      if( m_bMouseDown )
         hotSelect( pMS );

      return S_OK;
   }

   STDMETHOD(MouseMove)(MouseState *pMS)
   {
      if( m_bMouseDown )
         hotSelect( pMS );

      return S_OK;
   }

   STDMETHOD(MouseUp)(MouseState *pMS);
};

#endif //__CHOICEPOPUP_H_
